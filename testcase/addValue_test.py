from playwright.sync_api import expect

from public import base_unit
import unittest
from config import setting
from page.login_page import LoginPage
from page.value_page import ValuePage
from public.random_str import random_str



class AddValueTest(base_unit.BaseTest):

    def test_add_value(self):
        adv_name = "rta测试商户号"
        distance = ord('t') - ord('a') + 1

        try:
            login_page = LoginPage(self.page)
            login_page.login(setting.USERNAME, setting.PASSWORD)
            for i in range(distance):
                char = chr(ord('a') + i)
                value_page = ValuePage(self.page,adv_name)
                add =value_page.add_value(char)
                if add:
                    self.assertEqual(1,add,'未查询到新增的变量！！')
                else:
                    break

        except AssertionError:
            self.fail()


if __name__ == '__main__':
    unittest.main()

from playwright.sync_api import expect

from public import base_unit
import unittest
from config import setting
from page.login_page import LoginPage
from page.rtaId_page import RtaIdPage
from public.random_str import random_str



class AddRtaIdTest(base_unit.BaseTest):

    def test_add_rtaId(self):
        auto_rand = 'autotest_' + random_str(5)
        adv_name = "rta测试商户号"
        try:
            login_page = LoginPage(self.page)
            login_page.login(setting.USERNAME, setting.PASSWORD)

            rtaId_page = RtaIdPage(self.page,adv_name)
            add =rtaId_page.add_up_rtaId(auto_rand)
            self.assertEqual(1,add[0],'未查询到新增的rtaId！！')
            self.assertEqual("上线",add[1],'状态不正确！！')


        except AssertionError:
            self.fail()


if __name__ == '__main__':
    unittest.main()

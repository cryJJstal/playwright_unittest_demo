import time
import re
from page.base_page import Page



class ValuePage(Page):

    def __init__(self, page,adv_name):
        super().__init__(page)
        self.value_page = self.page.get_by_role('menuitem').locator('text="变量管理"')
        self.add =self.page.locator('text="新增变量"')
        self.advName_input =self.get_by_placeholder('商户号')
        self.advName_list = self.locator('li').filter(has_text=adv_name)
        self.valueName = self.page.get_by_label("创建变量").get_by_placeholder("请填写变量名")
        self.valueCode =self.page.get_by_label("创建变量").get_by_placeholder('变量code')
        self.mappingCode =self.get_by_placeholder("映射后code")
        self.valueType_input =self.get_by_placeholder("变量分类")
        self.valueType_list =self.locator('li').filter(has_text='模型变量').nth(1)
        self.dataType_input = self.get_by_placeholder("数据类型")
        self.dataType_list =self.locator('li').filter(has_text='浮点数')
        self.saveBtn = self.locator('text="提交"')
        self.box = self.page.get_by_role('dialog').locator('button').filter(has_text="确定")

        self.select_valueCode = self.page.get_by_role("textbox", name="请填写变量名")
        self.select_valueCode_list = self.locator('li')
        self.selectBtn = self.locator('text="查询"')
        self.table_data= self.locator('xpath=//div[@class="el-table__body-wrapper is-scrolling-none"]/table[@class="el-table__body"]/tbody/tr[@class="el-table__row"]')

    def add_value(self,char):
        self.value_page.click()
        self.add.click()
        self.advName_input.click()
        self.advName_list.click()
        self.valueName.fill(char+'变量')
        self.valueCode.fill(char+'_score')
        self.mappingCode.fill(char)
        self.valueType_input.click()
        self.valueType_list.click()
        self.dataType_input.click()
        self.dataType_list.click()
        self.saveBtn.click()
        with self.page.expect_response("*/api/v1/value/create*",timeout=10000) as create_rep:
            self.box.click()
        res = create_rep.value.json()
        if res['code'] != 0:
            print('创建变量不成功，错误信息:%s' % (res['message']))
        else:
            time.sleep(3)
            self.select_valueCode.click()
            self.select_valueCode_list.filter(has_text=char+'变量')
            self.selectBtn.click()
            self.page.wait_for_load_state('networkidle')
            count = self.table_data.count()
            return count
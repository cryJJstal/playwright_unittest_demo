import time
import re
from page.base_page import Page



class RtaIdPage(Page):

    def __init__(self, page,adv_name):
        super().__init__(page)
        self.rtaId_page = self.page.get_by_role('menuitem').locator('text="rta_id管理"')
        self.add =self.page.locator('text="新增RTA ID"')
        self.innerName_input =self.get_by_placeholder('渠道名称')
        self.innerName_list = self.locator("li").filter(has_text="广点通").nth(1)
        self.advName_input =self.get_by_placeholder('广告主名称')
        self.advName_list = self.locator('li').filter(has_text=adv_name).nth(1)
        self.rtaId = self.get_by_placeholder('RTAID')
        self.rtaName =self.get_by_placeholder('rta名称')
        self.saveBtn = self.locator('text="提交"')
        self.box = self.page.get_by_role('dialog').locator('button').filter(has_text="确定")


        self.select_rtaId = self.get_by_placeholder("请填写RTA ID")
        self.selectBtn = self.locator('text="查询"')
        self.table_data= self.locator('xpath=//div[@class="el-table__body-wrapper is-scrolling-none"]/table[@class="el-table__body"]/tbody/tr[@class="el-table__row"]')
        self.table_op = self.locator('xpath=//div[@class="el-table__fixed-body-wrapper"]/table[@class="el-table__body"]/tbody/tr[@class="el-table__row"]')

    def add_up_rtaId(self,auto_rand):
        self.rtaId_page.click()
        self.add.click()
        self.innerName_input.click()
        self.innerName_list.click()
        self.advName_input.click()
        self.advName_list.click()
        self.rtaId.fill(auto_rand)
        self.rtaName.fill(auto_rand)
        self.saveBtn.click()
        self.box.click()
        self.select_rtaId.fill(auto_rand)
        self.selectBtn.click()
        self.page.wait_for_load_state('networkidle')
        count = self.table_data.count()
        self.table_op.locator('td').last.locator('text="上线"').click()
        self.box.click()
        self.page.wait_for_load_state('networkidle')
        status = self.table_data.locator('td').nth(7).locator('xpath=//div/span/span[2]').inner_text()
        return count, status
import time

from page.base_page import Page


class LoginPage(Page):

    def __init__(self, page):
        super().__init__(page)
        self.username = self.get_by_placeholder('账号/邮箱/手机号')
        self.password = self.get_by_placeholder('请输入密码')
        self.code = self.get_by_placeholder('验证码')
        self.login_btn = self.locator('text="登陆"')


    def login(self, username, password):
        self.page.on('request', self.on_request)
        self.page.on('response', self.on_response)
        with self.page.expect_response("*/api/v1/user/getValidateCode") as res_info:
            self.goto()
        code = res_info.value.json()['data']
        self.username.fill(username)
        self.password.fill(password)
        self.code.fill(code)
        self.login_btn.click()
        time.sleep(5)
